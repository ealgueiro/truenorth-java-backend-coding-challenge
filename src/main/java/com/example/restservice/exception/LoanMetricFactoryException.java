package com.example.restservice.exception;

import com.example.restservice.util.LoanGeneratonUtil;

public class LoanMetricFactoryException extends Exception {
    public LoanMetricFactoryException() {
        super(String.format("Loan type must be either %s or %s", LoanGeneratonUtil.LOAN_TYPE_CONSUMER,
                LoanGeneratonUtil.LOAN_TYPE_STUDENT));
    }
}
