package com.example.restservice.service;

import com.example.restservice.exception.LoanMetricFactoryException;
import com.example.restservice.metrics.LoanMetricFactory;
import com.example.restservice.model.Loan;
import com.example.restservice.model.LoanMetric;
import com.example.restservice.util.LoanGeneratonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;

@Service
public class LoanService {

	// Private property which contains Factory Method pattern
	private final LoanMetricFactory loanMetricFactory;

	@Autowired
	public LoanService(LoanMetricFactory loanMetricFactory) {
		this.loanMetricFactory = loanMetricFactory;
	}

	public Loan getLoan(Long id) {
		return LoanGeneratonUtil.createLoan(id);
	}

	public LoanMetric calculateLoanMetric(Loan loan) throws LoanMetricFactoryException {
		// Use the LoanMetricFactory based on the loan type
		return loanMetricFactory.getInstance(loan).getLoanMetric(loan);
	}

	public LoanMetric calculateLoanMetric(Long loanId) throws LoanMetricFactoryException {
		// Call getLoan(loanId)
		Loan loan = getLoan(loanId);
		return loanMetricFactory.getInstance(loan).getLoanMetric(loan);
	}

	public Loan getMaxMonthlyPaymentLoan() {
		List<Loan> allLoans = LoanGeneratonUtil.getRandomLoans(20L);

		// get the loan with the max monthly payment
		return allLoans.stream()
				.max(Comparator.comparing(Loan::getRequestedAmount)
						.thenComparing(Loan::getTermMonths))
				.orElse(null);
	}
}
