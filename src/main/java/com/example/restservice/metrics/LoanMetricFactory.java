package com.example.restservice.metrics;

import com.example.restservice.exception.LoanMetricFactoryException;
import com.example.restservice.metrics.impl.ConsumerLoanMetricCalculator;
import com.example.restservice.metrics.impl.StudentLoanMetricCalculator;
import com.example.restservice.util.LoanGeneratonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.restservice.model.Loan;

@Service
public class LoanMetricFactory {
   // Factory Method Pattern

   private final ConsumerLoanMetricCalculator consumerLoanMetricCalculator;
   private final StudentLoanMetricCalculator studentLoanMetricCalculator;

   @Autowired
   public LoanMetricFactory(ConsumerLoanMetricCalculator consumerLoanMetricCalculator, StudentLoanMetricCalculator studentLoanMetricCalculator) {
      this.consumerLoanMetricCalculator = consumerLoanMetricCalculator;
      this.studentLoanMetricCalculator = studentLoanMetricCalculator;
   }

   public ILoanMetricCalculator getInstance(Loan loan) throws LoanMetricFactoryException {
      // Builds the instance if the loan exist, otherwise throws an exception
      if (loan == null) {
         throw new LoanMetricFactoryException();
      }

      // Validates loan type (consumer  or student) and return the correct instance, otherwise throws an exception
      if (loan.getType().equals(LoanGeneratonUtil.LOAN_TYPE_CONSUMER)) {
         return consumerLoanMetricCalculator;
      } else if (loan.getType().equals(LoanGeneratonUtil.LOAN_TYPE_STUDENT)) {
         return studentLoanMetricCalculator;
      } else {
         throw new LoanMetricFactoryException();
      }
   }
}
