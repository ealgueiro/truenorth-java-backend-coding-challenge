package com.example.restservice.metrics;

import com.example.restservice.model.Loan;
import com.example.restservice.model.LoanMetric;
import com.example.restservice.util.LoanGeneratonUtil;

public interface ILoanMetricCalculator {

	/**
	 * Validates if a loan is supported to calculate metrics
	 *
	 * @param loan
	 */
	default boolean isSupported(Loan loan) {
		// Validate if the loan type is supported
		if (loan == null || loan.getType() == null) return false;

		if (loan.getType().equals(LoanGeneratonUtil.LOAN_TYPE_CONSUMER) ||
				loan.getType().equals(LoanGeneratonUtil.LOAN_TYPE_STUDENT)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Calculates the Loan Metric of a Loan entity
	 *
	 * @param loan
	 */
	LoanMetric getLoanMetric(Loan loan);
}
