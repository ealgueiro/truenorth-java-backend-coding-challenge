package com.example.restservice.metrics.impl;

import org.springframework.stereotype.Component;

import com.example.restservice.metrics.ILoanMetricCalculator;
import com.example.restservice.model.Loan;
import com.example.restservice.model.LoanMetric;

@Component
public class ConsumerLoanMetricCalculator implements ILoanMetricCalculator {

	@Override
	public LoanMetric getLoanMetric(Loan loan) {

		double monthlyInterestRate = (loan.getAnnualInterest() / 12) / 100;
		double pow = Math.pow((1 + monthlyInterestRate), ((-1) * loan.getTermMonths()));
		double monthlyPayment = (loan.getRequestedAmount() * monthlyInterestRate) / (1 - pow);
		return new LoanMetric(monthlyInterestRate, monthlyPayment);

	}

}
