package com.example.restservice.metrics.impl;

import com.example.restservice.metrics.ILoanMetricCalculator;
import com.example.restservice.model.Loan;
import com.example.restservice.model.LoanMetric;
import org.springframework.stereotype.Component;

@Component
public class StudentLoanMetricCalculator implements ILoanMetricCalculator {

	@Override
	public LoanMetric getLoanMetric(Loan loan) {

		double monthlyInterestRate = (loan.getAnnualInterest() / 12) / 100;
		double pow = Math.pow((1 + monthlyInterestRate), ((-1) * loan.getTermMonths()));
		double monthlyPayment = 0.8 * (loan.getRequestedAmount() * monthlyInterestRate) / (1 - pow);
		return new LoanMetric(monthlyInterestRate, monthlyPayment);

	}

	@Override
	public boolean isSupported(Loan loan) {

		if (loan == null || loan.getBorrower() == null) return false;

		return ILoanMetricCalculator.super.isSupported(loan) && //Calls super interface implementation
				loan.getBorrower().getAge() > 18 &&
				loan.getBorrower().getAge() < 30;
	}
}
