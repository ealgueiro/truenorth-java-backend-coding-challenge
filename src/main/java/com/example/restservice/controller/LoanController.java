package com.example.restservice.controller;

import com.example.restservice.exception.LoanMetricFactoryException;
import com.example.restservice.model.Loan;
import com.example.restservice.model.LoanMetric;
import com.example.restservice.service.LoanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/loans")
public class LoanController {

    private final LoanService loanService;

    @Autowired
    public LoanController(LoanService loanService) {
        this.loanService = loanService;
    }

    @GetMapping("/{loanId}")
    public Loan getLoan(@PathVariable Long loanId) {
        return loanService.getLoan(loanId);
    }

    @GetMapping("/{loanId}/metric")
    public LoanMetric calculateLoanMetric(@PathVariable Long loanId)
            throws LoanMetricFactoryException {
        return loanService.calculateLoanMetric(loanId);
    }

    @GetMapping("/metric")
    public LoanMetric calculateLoanMetric(
            @RequestBody Loan loan)
            throws LoanMetricFactoryException {
        return loanService.calculateLoanMetric(loan);
    }

    @GetMapping("/max_monthly_payment")
    public Loan getMaxMonthlyPaymentLoan() {
        return loanService.getMaxMonthlyPaymentLoan();
    }
}