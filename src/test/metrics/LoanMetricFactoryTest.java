package metrics;

import com.example.restservice.exception.LoanMetricFactoryException;
import com.example.restservice.metrics.ILoanMetricCalculator;
import com.example.restservice.metrics.LoanMetricFactory;
import com.example.restservice.metrics.impl.ConsumerLoanMetricCalculator;
import com.example.restservice.metrics.impl.StudentLoanMetricCalculator;
import com.example.restservice.model.Loan;
import com.example.restservice.util.LoanGeneratonUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class LoanMetricFactoryTest {

    private final ConsumerLoanMetricCalculator consumerLoanMetricCalculator = Mockito.mock(ConsumerLoanMetricCalculator.class);
    private final StudentLoanMetricCalculator studentLoanMetricCalculator = Mockito.mock(StudentLoanMetricCalculator.class);
    private final LoanMetricFactory loanMetricFactory = new LoanMetricFactory(consumerLoanMetricCalculator, studentLoanMetricCalculator);

    @Test
    public void test_consumer_instance_when_is_ok() {
        // GIVEN
        Loan loan = LoanGeneratonUtil.createLoan(1L);

        // WHEN
        ILoanMetricCalculator result = null;
        try {
            result = loanMetricFactory.getInstance(loan);
        } catch (LoanMetricFactoryException e) {
            Assertions.fail();
        }

        // THEN
        Assertions.assertEquals(true, result instanceof ConsumerLoanMetricCalculator);
    }

    @Test
    public void test_student_instance_when_is_ok() {
        // GIVEN
        Loan loan = LoanGeneratonUtil.createLoan(2L);

        // WHEN
        ILoanMetricCalculator result = null;
        try {
            result = loanMetricFactory.getInstance(loan);
        } catch (LoanMetricFactoryException e) {
            Assertions.fail();
        }

        // THEN
        Assertions.assertEquals(true, result instanceof StudentLoanMetricCalculator);
    }

    @Test
    public void test_getInstance_when_is_not_ok() {
        // WHEN
        ILoanMetricCalculator result = null;
        try {
            result = loanMetricFactory.getInstance(null);
            Assertions.fail();
        } catch (LoanMetricFactoryException e) {
            Assertions.assertEquals("Loan type is wrong!", e.getMessage());
        }
    }
}
