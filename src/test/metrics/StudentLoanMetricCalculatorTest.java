package metrics;

import com.example.restservice.metrics.impl.ConsumerLoanMetricCalculator;
import com.example.restservice.metrics.impl.StudentLoanMetricCalculator;
import com.example.restservice.model.Borrower;
import com.example.restservice.model.Loan;
import com.example.restservice.model.LoanMetric;
import com.example.restservice.util.LoanGeneratonUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class StudentLoanMetricCalculatorTest {

    @Test
    public void test_calculator_when_is_ok() {
        // GIVEN
        Loan loan = createStudentLoan();
        StudentLoanMetricCalculator calculator = new StudentLoanMetricCalculator();

        // WHEN
        LoanMetric result = calculator.getLoanMetric(loan);

        // THEN
        Assertions.assertEquals(354.5648820220625, result.getMonthlyPayment());
        Assertions.assertEquals(0.005, result.getMonthlyInterestRate());
    }

    private Loan createStudentLoan() {
        String loanType = LoanGeneratonUtil.LOAN_TYPE_STUDENT;
        Borrower borrower = new Borrower();
        borrower.setName("James Gosling");
        borrower.setAge(44);
        borrower.setAnnualIncome(10000.00);
        borrower.setDelinquentDebt(false);
        borrower.setAnnualDebt(3000.00);
        borrower.setCreditHistory(6);

        Loan loan = new Loan();
        loan.setLoanId(1L);
        loan.setRequestedAmount(10000.00);
        loan.setTermMonths(24);
        loan.setAnnualInterest(6.0);
        loan.setType(loanType);
        loan.setBorrower(borrower);

        return loan;
    }

}
