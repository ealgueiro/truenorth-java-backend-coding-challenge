package controller;

import com.example.restservice.controller.LoanController;
import com.example.restservice.exception.LoanMetricFactoryException;
import com.example.restservice.model.Loan;
import com.example.restservice.model.LoanMetric;
import com.example.restservice.service.LoanService;
import com.example.restservice.util.LoanGeneratonUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

public class LoanControllerTest {

    private final LoanService loanService = Mockito.mock(LoanService.class);
    private final LoanController loanController = new LoanController(loanService);

    @Test
    public void test_getLoan_when_is_ok() {
        // GIVEN
        Loan loan = LoanGeneratonUtil.createLoan(1L);
        Mockito.when(loanService.getLoan(Mockito.anyLong())).thenReturn(loan);

        // WHEN
        Loan result = loanController.getLoan(1L);

        // THEN
        Assertions.assertEquals(1L, (long) result.getLoanId());
    }

    @Test
    public void test_calculateLoanMetric_parameter_long_when_is_ok() {
        // GIVEN
        LoanMetric loanMetric = new LoanMetric(1.0,1.0);

        // WHEN
        LoanMetric result = null;
        try {
            Mockito.when(loanService.calculateLoanMetric(Mockito.anyLong())).thenReturn(loanMetric);

            result = loanController.calculateLoanMetric(1L);
        } catch (LoanMetricFactoryException e) {
            Assertions.fail();
        }

        // THEN
        Assertions.assertEquals(1.0, result.getMonthlyInterestRate());
    }

    @Test
    public void test_calculateLoanMetric_parameter_loan_when_is_ok() {
        // GIVEN
        Loan loan = LoanGeneratonUtil.createLoan(1L);
        LoanMetric loanMetric = new LoanMetric(1.0,1.0);

        // WHEN
        LoanMetric result = null;
        try {
            Mockito.when(loanService.calculateLoanMetric(Mockito.any(Loan.class))).thenReturn(loanMetric);

            result = loanController.calculateLoanMetric(loan);
        } catch (LoanMetricFactoryException e) {
            Assertions.fail();
        }

        // THEN
        Assertions.assertEquals(1.0, result.getMonthlyInterestRate());
    }

    @Test
    public void test_getMaxMonthlyPaymentLoan_when_is_ok() {
        // GIVEN
        Loan loan = LoanGeneratonUtil.createLoan(1L);
        Mockito.when(loanService.getMaxMonthlyPaymentLoan()).thenReturn(loan);

        // WHEN
        Loan result = loanController.getMaxMonthlyPaymentLoan();

        // THEN
        Assertions.assertEquals(1L, (long) result.getLoanId());
    }

}
