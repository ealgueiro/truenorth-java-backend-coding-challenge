package service;

import com.example.restservice.exception.LoanMetricFactoryException;
import com.example.restservice.metrics.LoanMetricFactory;
import com.example.restservice.metrics.impl.ConsumerLoanMetricCalculator;
import com.example.restservice.model.Loan;
import com.example.restservice.model.LoanMetric;
import com.example.restservice.service.LoanService;
import com.example.restservice.util.LoanGeneratonUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

public class LoanServiceTest {

    private static LoanMetricFactory loanMetricFactory = Mockito.mock(LoanMetricFactory.class);

    private static ConsumerLoanMetricCalculator consumerLoanMetricCalculator = Mockito.mock(ConsumerLoanMetricCalculator.class);;

    LoanService loanService = new LoanService(loanMetricFactory);

    @BeforeAll
    public static void setup()  {
        try {
            Mockito.when(loanMetricFactory.getInstance(Mockito.any())).thenReturn(consumerLoanMetricCalculator);
        } catch (LoanMetricFactoryException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void test_getLoan_when_is_ok() throws LoanMetricFactoryException {
        // GIVEN
        Loan loan = LoanGeneratonUtil.createLoan(1L);
        Mockito.when(consumerLoanMetricCalculator.getLoanMetric(Mockito.any())).thenReturn(new LoanMetric(1.0,1.0));

        // WHEN
        Loan result = loanService.getLoan(loan.getLoanId());


        // THEN
        Assertions.assertEquals(1L, (long) result.getLoanId());
    }

    @Test
    public void test_calculateLoanMetric_when_is_ok() throws LoanMetricFactoryException {
        // GIVEN
        Mockito.when(consumerLoanMetricCalculator.getLoanMetric(Mockito.any())).thenReturn(new LoanMetric(1.0,1.0));

        // WHEN
        LoanMetric result = null;
        try {
            result = loanService.calculateLoanMetric(1L);
        } catch (LoanMetricFactoryException e) {
            Assertions.fail();
        }


        // THEN
        Assertions.assertEquals(1.0, result.getMonthlyInterestRate());
    }

    @Test
    public void test_calculateLoanMetric_parameter_loan_when_is_ok() throws LoanMetricFactoryException {
        // GIVEN
        Loan loan = LoanGeneratonUtil.createLoan(1L);
        Mockito.when(consumerLoanMetricCalculator.getLoanMetric(Mockito.any())).thenReturn(new LoanMetric(1.0,1.0));

        // WHEN
        LoanMetric result = null;
        try {
            result = loanService.calculateLoanMetric(loan);
        } catch (LoanMetricFactoryException e) {
            Assertions.fail();
        }


        // THEN
        Assertions.assertEquals(1.0, result.getMonthlyInterestRate());
    }

    @Test
    public void test_getMaxMonthlyPaymentLoan_when_is_ok() {
        // WHEN
        Loan result = loanService.getMaxMonthlyPaymentLoan();

        // THEN
        Assertions.assertEquals(20L, (long) result.getLoanId());
    }

}
